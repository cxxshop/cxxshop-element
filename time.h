#ifndef TIME_H
#define TIME_H

#include "api/element.h"

namespace nstd::impl
{

class TimeImpl
{
  public:
    std::string timestamp();
};
} // namespace nstd::impl

#endif // TIME_H