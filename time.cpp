#include "element/time.h"

#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>

using namespace nstd;

/* Time */

element::Time::Time() : m_impl(new impl::TimeImpl()) {}
element::Time::~Time() { delete m_impl; }
std::string element::Time::timestamp() { return m_impl->timestamp(); }

/* TimeImpl */

std::string impl::TimeImpl::timestamp()
{
    auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

    std::ostringstream oss;
    oss << std::put_time(std::localtime(&now), "%Y-%m-%d %H:%M:%S") << "." << std::setfill('0')
        << std::setw(3)
        << std::chrono::duration_cast<std::chrono::milliseconds>(
               std::chrono::system_clock::now().time_since_epoch())
                   .count() %
               1000;

    return oss.str();
}
